public class Main {

    // функция, для которой считаем определенный интеграл - y = x^2 (парабола), y = sin(x) * 2
    public static double f(double x) {
        return  x *x  ;
    }

    public static double integralBySimpson (double a, double b, int n) {
        double width = (b - a) / n;
        double sum = 0;
        for (double x = a; x <= b; x += width) {
            double y = f(x);
            int x0 = 0;
            int y0 = 0;
            int x1, x3, x5, x7, x9 = 1;
            int x2, x4, x6, x8 = 4;
            float...
            //я понимаю что там нужна переменная float
            //завис в решении....
            // формула Симпсона S= (1/2y0+2y1+y2+2y3+y4+......+2yn-1+1/2Yn)*2 /3

            return 0.0;
        }
    }}
    public static void main (String[] args) {
        // массив с примерами количеств разбиений исходного диапазона
        int[] ns = {10, 100, 1000, 10_000, 50_000, 100_000,
                150_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        double from = 0;
        double to = 10;
        double realValue = 3.67814305815;

        // массив с результатами работы функции integral для разных разбиений, в ys[i] находится значение для разбиения ns[i]
        double[] ys = new double[ns.length];
        // считаем интегралы для всех разбиений на промежутке от 0 до 10
        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralBySimpson (from, to, ns[i]);
        }
        // вывод результатов
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("|N = %10d| Y = %10.4f| EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
    }


}
