

/**
 * 30.10.2021
 * 12. src.main.java.Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bus {
    private int number;
    private String model;
    private Driver driver;
    private boolean move;


    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.move = false;
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (move) {
            System.out.println("Автобус в движении");
            return;
        }
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        if (move) {
            System.out.println("Автобус движется, водитель занят!");
            return;
        }
        this.driver = driver;
        driver.setBus(this);
    }
    public boolean getMove() {
        return this.move;
    }

    public void setMove(boolean bool) {
        this.move = bool;
    }

    public Passenger[] getPassengers() {
        return this.passengers;
    }

    public void leavePassenger(Passenger passenger, int index) {
        if (passenger.getName().equals(this.passengers[index].getName())) {
                this.passengers[index] = null;
                this.count--;
                replace();
                return;
        }
        System.out.println("Такого пассажира  нет");
    }

    public int getCount() {
        return this.count;
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    private void replace() {
        for (int i = 0; i < passengers.length - 1; i++) {
            if (passengers[i] == null) {
                passengers[i] = passengers[i + 1];
                passengers[i + 1] = null;
            }
        }
    }

}
