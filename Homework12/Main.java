public class Main {

    public static void main(String[] args) {
        Driver driver = new Driver("Семен", 10);

        Bus bus = new Bus(99, "Нефаз", 6);
	    Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Айрат");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger4.goToBus(bus);
        passenger5.goToBus(bus);
        passenger6.goToBus(bus);

        bus.setDriver(driver);

        driver.drive();

        Driver driver1 = new Driver("Kamil", 25);
        bus.setDriver(driver1);
        passenger1.leaveBus();
        driver.stopBus();
        for (int i = 0; i < bus.getPassengers().length; i++) {
            System.out.println(bus.getPassengers()[i].getName());
        }
        System.out.println("After:");
        passenger1.leaveBus();
        for (int i = 0; i < bus.getPassengers().length - 1; i++) {
            System.out.println(bus.getPassengers()[i].getName());
        }
    }
}
