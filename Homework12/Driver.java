/**
 * 30.10.2021
 * 12. src.main.java.Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ! Автобус едет");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive() {
        bus.setMove(true);
        for (int i = 0; i < bus.getPassengers().length; i++) {
            System.out.println(bus.getPassengers()[i].getName());
        }
    }

    public void stopBus() {
        bus.setMove(false);
    }


}
